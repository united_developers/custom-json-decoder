import Foundation

public final class CustomJSONDecoder {
    public init() {}
    
    public func decode<T: Decodable>(_ type: T.Type, from data: Data) throws -> T {
        guard let string = String(data: data, encoding: .utf8) else {
            throw DecodingError.dataCorrupted(.init(codingPath: [], debugDescription: "Given data is not valid UTF-8 string"))
        }
        
        let parser = Parser(string: string)
        let parsingValue = try parser.parse()
        let topLevelDecoder = CustomDecoder(codingPath: [], value: parsingValue)
        
        return try T(from: topLevelDecoder)
    }
}
