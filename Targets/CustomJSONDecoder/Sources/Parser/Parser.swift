import Foundation

final class Parser {
    private let characters: [Character]
    
    private var currentIndex = 0
    private var scopesStack: [ParsingScope] = []
    private var currentScope: ParsingScope = NoScope()
    
    init(string: String) {
        characters = Array(string)
    }
    
    func parse() throws -> Object {
        while currentIndex < characters.count {
            try apply(currentScope.tryModify(with: characters[currentIndex]))
        }
        
        guard let noScope = currentScope as? NoScope, let value = noScope.value else {
            throw ParsingError.internalError
        }
        
        return value
    }
    
    private func apply(_ scopeChange: ScopeModificationResult) throws {
        switch scopeChange {
        case .noModification:
            currentIndex += 1
        case .modifiedScope(let scope):
            currentScope = scope
            currentIndex += 1
        case .newScope(let scope):
            scopesStack.append(currentScope)
            currentScope = scope
            currentIndex += 1
        case .returnToParent(let value, let repeatChar):
            guard let parent = scopesStack.popLast() as? ContainerScope else {
                throw ParsingError.internalError
            }
            try apply(parent.accept(value))
            currentIndex -= repeatChar ? 1 : 0
        }
    }
}
