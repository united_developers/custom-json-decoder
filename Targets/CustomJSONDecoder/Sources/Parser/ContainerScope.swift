import Foundation

protocol ContainerScope: ParsingScope {
    func accept(_ value: Object) throws -> ScopeModificationResult
}

struct ArrayScope: ContainerScope {
    var values: [Object]
    var isSearchingValue = true
    
    func isValidCharacter(_ char: Character) -> Bool {
        return isSearchingValue ? Set(", \n\t[]\"{tfn1234567890+-").contains(char) : char == ","
        
    }
    
    mutating func modify(with char: Character) -> ScopeModificationResult {
        switch (char, isSearchingValue) {
        case ("[", true):
            return .newScope(ArrayScope(values: values))
        case ("\"", true):
            return .newScope(StringScope(chars: [char], isEscaping: false))
        case (_, true) where Set("+-1234567890").contains(char):
            return .newScope(NumberScope(chars: [char]))
        case ("t", true), ("f", true), ("n", true):
            return .newScope(LiteralScope(startingChar: char))
        case ("{", true):
            return .newScope(DictionaryScope())
        case (",", false):
            isSearchingValue = true
            return.noModification
        case (" ", true), ("\t", true), ("\n", true):
            return .noModification
        default:
            return .noModification
    
        }
    }
    
    func accept(_ value: Object) throws -> ScopeModificationResult {
        return .modifiedScope(ArrayScope(values: values + [value], isSearchingValue: false))
    }
}

struct DictionaryScope: ContainerScope, ParsingScope {
    private enum Phase {
        private static let searchingKeyChars = Set(" \n\t\"}")
        private static let searchingColumnChars = Set(" \n\t:")
        private static let searchingValueChars = Set(" \n\t\"tfn1234567890-+{[")
        private static let searchingCommaChars = Set(" \n\t,}")
        
        case searchingKey
        case searchingColumn(key: String)
        case searchingValue(key: String)
        case searchingComma
        
        var allowedChars: Set<Character> {
            switch self {
            case .searchingKey:
                return Phase.searchingKeyChars
            case .searchingColumn:
                return Phase.searchingColumnChars
            case .searchingValue:
                return Phase.searchingValueChars
            case .searchingComma:
                return Phase.searchingCommaChars
            }
        }
    }
    
    private static let numbers = Set("-+1234567890")
    private static let literalStart = Set("ftn")
    
    private var values: [String: Object]
    private var phase: Phase
    
    init() {
        values = [:]
        phase = .searchingKey
    }
    
    private init(values: [String: Object], phase: Phase) {
        self.values = values
        self.phase = phase
    }
    
    func isValidCharacter(_ char: Character) -> Bool {
        return phase.allowedChars.contains(char)
    }
    
    mutating func modify(with char: Character) -> ScopeModificationResult {
        switch (phase, char) {
        case (.searchingKey, "\""), (.searchingValue, "\""):
            return .newScope(StringScope(chars: [char], isEscaping: false))
        case (.searchingColumn(let key), ":"):
            phase = .searchingValue(key: key)
            return .noModification
        case (.searchingValue, _) where DictionaryScope.numbers.contains(char):
            return .newScope(NumberScope(chars: [char]))
        case (.searchingValue, _) where DictionaryScope.literalStart.contains(char):
            return .newScope(LiteralScope(startingChar: char))
        case (.searchingValue, "["):
            return .newScope(ArrayScope(values: []))
        case (.searchingValue, "{"):
            return .newScope(DictionaryScope())
        case (.searchingComma, ","):
            phase = .searchingKey
            return .noModification
        case (.searchingComma, "}"), (.searchingKey, "}"):
            return .returnToParent(value: .dictionary(values), repeateValue: false)
        default:
            return .noModification
        }
    }
    
    func accept(_ value: Object) throws -> ScopeModificationResult {
        if case .searchingValue(let key) = phase {
            var newValues = values
            newValues[key] = value
            return .modifiedScope(DictionaryScope(values: newValues, phase: .searchingComma))
        } else if case .string(let key) = value, case .searchingKey = phase, values[key] == nil {
            return .modifiedScope(DictionaryScope(values: values, phase: .searchingColumn(key: key)))
        } else {
            throw ParsingError.internalError
        }
    }
}

struct NoScope: ContainerScope {
    private static let allowedCharset = Set(" \n\t{[")
    private(set) var value: Object?
    
    init() {
        value = nil
    }
    
    private init(value: Object? = nil) {
        self.value = value
    }
    
    func isValidCharacter(_ char: Character) -> Bool {
        return NoScope.allowedCharset.contains(char)
    }
    
    func modify(with char: Character) -> ScopeModificationResult {
        switch char {
        case "{":
            return .newScope(DictionaryScope())
        case "[":
            return .newScope(ArrayScope(values: []))
        default:
            return .noModification
        }
    }
    
    func accept(_ value: Object) throws -> ScopeModificationResult {
        if self.value == nil {
            return .modifiedScope(NoScope(value: value))
        } else {
            throw ParsingError.internalError
        }
    }
}
