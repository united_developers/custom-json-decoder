import Foundation

enum ScopeModificationResult {
    case noModification
    case modifiedScope(ParsingScope)
    case newScope(ParsingScope)
    case returnToParent(value: Object, repeateValue: Bool)
}

protocol ParsingScope {
    func isValidCharacter(_ char: Character) -> Bool
    mutating func modify(with char: Character) -> ScopeModificationResult
    mutating func tryModify(with char: Character) throws -> ScopeModificationResult
}

extension ParsingScope {
    mutating func tryModify(with char: Character) throws -> ScopeModificationResult {
        guard isValidCharacter(char) else {
            throw ParsingError.internalError
        }
        return modify(with: char)
    }
}

struct StringScope: ParsingScope {
    var chars: [Character]
    var isEscaping: Bool
    
    func isValidCharacter(_ char: Character) -> Bool {
        return true
    }
    
    mutating func modify(with char: Character) -> ScopeModificationResult {
        switch (char, isEscaping) {
        case ("\"", false):
            return .returnToParent(value: .string(String(chars)), repeateValue: false)
        case ("\"", true), ("\\", false):
            chars.append(char)
            isEscaping = false
            return .modifiedScope(StringScope(chars: chars, isEscaping: false))
        case ("\\", false):
            isEscaping = true
            return .modifiedScope(StringScope(chars: chars, isEscaping: true))
        default:
            chars.append(char)
            return .modifiedScope(StringScope(chars: chars, isEscaping: false))
        }
    }
}

struct NumberScope: ParsingScope {
    private static let numbers = Set("1234567890")
    private static let terminationChars = Set(" ,}]\n\t")
    
    private var chars: [Character]
    private var hasDot: Bool = false
    
    init(chars: [Character]) {
        self.chars = chars
    }
    
    func isValidCharacter(_ char: Character) -> Bool {
        return ((char == "-" || char == "+") && chars.isEmpty)
            || (char == "." && !hasDot)
            || NumberScope.numbers.contains(char)
            || NumberScope.terminationChars.contains(char)
    }
    
    mutating func modify(with char: Character) -> ScopeModificationResult {
        if char == "." {
            hasDot = true
            chars.append(char)
            return .noModification
        } else if !NumberScope.terminationChars.contains(char) {
            chars.append(char)
            return .noModification
        } else {
            return .returnToParent(value: .number(String(chars)), repeateValue: true)
        }
    }
}

struct LiteralScope: ParsingScope {
    private var string: String
    
    init(startingChar: Character) {
        string = String(startingChar)
    }
    
    func isValidCharacter(_ char: Character) -> Bool {
        fatalError()
    }
    
    func modify(with char: Character) -> ScopeModificationResult {
        fatalError()
    }
    
    mutating func tryModify(with char: Character) throws -> ScopeModificationResult {
        string.append(char)
        
        guard ["true", "false", "null"].contains(where: {
            $0.hasPrefix(string)
        }) else {
            throw ParsingError.internalError
        }
        
        if let bool = Bool(string) {
            return .returnToParent(value: .boolean(bool), repeateValue: false)
        } else if string == "null" {
            return .returnToParent(value: .null, repeateValue: false)
        } else {
            return .noModification
        }
    }
}


