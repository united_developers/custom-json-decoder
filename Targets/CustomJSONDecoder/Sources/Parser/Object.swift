import Foundation

enum Object {
    case string(String)
    case boolean(Bool)
    case null
    case number(String)
    indirect case dictionary([String: Object])
    indirect case array([Object])
}
