import Foundation

struct CustomSingleValueContainer: CodingPathHolder {

    let codingPath: [CodingKey]
    let value: Object

    init(codingPath: [CodingKey], value: Object) {
        self.codingPath = codingPath
        self.value = value
    }

    private func nonNullValue() throws -> Object {
        if case .null = value {
            throw DecodingError.valueNotFoundError(of: Bool.self, in: self)
        }

        return value
    }

    private func number<T: LosslessStringConvertible>() throws -> T {
        guard case let .number(string) = try nonNullValue() else {
            throw DecodingError.typeMismatchError(of: T.self, in: self)
        }
        
        guard let number = T(string) else {
            throw DecodingError.dataCorruptedError(in: self, debugDescription: "Can't parse \(T.self)")
        }
        
        return number
    }


    private func decoder() throws -> Decoder {
        return CustomDecoder(codingPath: codingPath, value: value)
    }

    private func decimal() throws -> Decimal {
        guard case let .number(string) = try nonNullValue() else {
            throw DecodingError.typeMismatchError(of: Decimal.self, in: self)
        }
        guard let number = Decimal(string: string) else {
            throw DecodingError.dataCorruptedError(in: self, debugDescription: "Can't parse \(Decimal.self)")
        }
        
        return number
    }
}

extension CustomSingleValueContainer: SingleValueDecodingContainer {
    func decodeNil() -> Bool {
        if case .null = value {
            return true
        } else {
            return false
        }
    }

    func decode(_ type: Bool.Type) throws -> Bool {
        if case .boolean(let bool) = try nonNullValue() {
            return bool
        } else {
            throw DecodingError.typeMismatch(Bool.self, .init(codingPath: codingPath, debugDescription: ""))
        }
    }

    func decode(_ type: String.Type) throws -> String {
        if case .string(let string) = try nonNullValue() {
            return string
        } else {
            throw DecodingError.typeMismatch(String.self, .init(codingPath: codingPath, debugDescription: ""))
        }
    }

    func decode(_ type: Double.Type) throws -> Double {
        return try number()
    }

    func decode(_ type: Float.Type) throws -> Float {
        return try number()
    }

    func decode(_ type: Int.Type) throws -> Int {
        return try number()
    }

    func decode(_ type: Int8.Type) throws -> Int8 {
        return try number()
    }

    func decode(_ type: Int16.Type) throws -> Int16 {
        return try number()
    }

    func decode(_ type: Int32.Type) throws -> Int32 {
        return try number()
    }

    func decode(_ type: Int64.Type) throws -> Int64 {
        return try number()
    }

    func decode(_ type: UInt.Type) throws -> UInt {
        return try number()
    }

    func decode(_ type: UInt8.Type) throws -> UInt8 {
        return try number()
    }

    func decode(_ type: UInt16.Type) throws -> UInt16 {
        return try number()
    }

    func decode(_ type: UInt32.Type) throws -> UInt32 {
        return try number()
    }

    func decode(_ type: UInt64.Type) throws -> UInt64 {
        return try number()
    }

    func decode<T>(_ type: T.Type) throws -> T where T : Decodable {
        return type == Decimal.self ? try decimal() as! T : try T(from: decoder())
    }
}
