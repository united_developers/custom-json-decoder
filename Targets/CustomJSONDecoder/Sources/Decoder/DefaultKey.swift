import Foundation

struct DefaultKey: CodingKey {

    public var stringValue: String

    init?(stringValue: String) {
        self.stringValue = stringValue
    }

    init?(intValue: Int) {
        stringValue = "\(intValue)"
    }

    public var intValue: Int? {
        return Int(stringValue)
    }
}
