import Foundation

struct CustomDecoder: Decoder {
    var codingPath: [CodingKey]
    var userInfo: [CodingUserInfoKey : Any] = [:]
    
    let value: Object
    
    func container<Key>(keyedBy type: Key.Type) throws -> KeyedDecodingContainer<Key> where Key : CodingKey {
        guard case let .dictionary(dict) = value else {
            throw DecodingError.typeMismatch(Dictionary<String, Any>.self, .init(codingPath: codingPath, debugDescription: ""))
        }
        return KeyedDecodingContainer<Key>.init(CustomKeyedContainer(codingPath: codingPath, dict: dict))
    }
    
    func unkeyedContainer() throws -> UnkeyedDecodingContainer {
        guard case let .array(values) = value else {
            throw DecodingError.typeMismatch(Dictionary<String, Any>.self, .init(codingPath: codingPath, debugDescription: ""))
        }
        
        return CustomUnkeyedContainer(codingPath: codingPath, values: values)
    }
    
    func singleValueContainer() throws -> SingleValueDecodingContainer {
        return CustomSingleValueContainer(codingPath: codingPath, value: value)
    }
}

private extension Array {
    func asDictionary() -> [String: Element] {
        var dict: [String: Element] = [:]

        for (index, element) in enumerated() {
            dict["\(index)"] = element
        }

        return dict
    }
}
