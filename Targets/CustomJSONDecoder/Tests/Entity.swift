import Foundation

struct Entity: Decodable {
    let string: String
    let integer: Int
    let double: Double
    let decimal: Decimal
    let bool: Bool
    let date: Date
}
