import XCTest
import CustomJSONDecoder

final class CustomJSONDecoderTests: XCTestCase {
    var decoder: CustomJSONDecoder!
    
    // TODO: add date parsing (TDD)
    
    override func setUp() {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-dd-MM HH:mm:ss"
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        
        decoder = CustomJSONDecoder()
    }
    
    override func tearDown() {}
    
    func testInvalidData() {
        do {
            _ = try decoder.decode(Entity.self, from: Data())
            XCTAssert(false, "Error wasn't thrown on invalid data")
        } catch {
            XCTAssert(true)
        }
    }
    
    func testInvalidString() {
        do {
            _ = try decoder.decode(Entity.self, from: "Not valid string".data(using: .utf8)!)
            XCTAssert(false, "Error wasn't thrown on invalid string")
        } catch {
            XCTAssert(true)
        }
    }
    
    func testValidDictionaryString() {
        let testData = """
                {
                    "string": "A",
                    "double": 2.88,
                    "integer": 8,
                    "decimal": 80.6,
                    "null": null,
                    "bool": true
                }
            """.data(using: .utf8)!
        do {
            let entry = try decoder.decode(Entity.self, from: testData)
            
            XCTAssertEqual(entry.string, "A")
            XCTAssertEqual(entry.double, 2.88)
            XCTAssertEqual(entry.integer, 8)
            XCTAssertEqual(entry.decimal, 80.6)
        } catch {
            XCTAssert(false, "Error during decoding: \(error)")
        }
    }
    
    func testValidArrayString() {
        let testData = """
                [
                    {
                        "string": "A",
                        "double": 2.88,
                        "integer": 8,
                        "decimal": 80.6,
                        "null": null,
                        "bool": true
                    },
                    {
                        "string": "B",
                        "double": 2.88,
                        "integer": 8,
                        "decimal": 80.6,
                        "null": null,
                        "bool": true
                    }
                ]
            """.data(using: .utf8)!
        do {
            let array = try decoder.decode([Entity].self, from: testData)
            
            XCTAssertEqual(array.count, 2)
            XCTAssertEqual(array.first?.string, "A")
            XCTAssertEqual(array.last?.string, "B")
        } catch {
            XCTAssert(false, "Error during decoding: \(error)")
        }
    }
    
    func testMissingKey() {
        let testData = """
                {
                    "string": "A",
                    "integer": 8,
                    "decimal": 80.6,
                    "null": null,
                    "bool": true
                }
            """.data(using: .utf8)!
        do {
            _ = try decoder.decode(Entity.self, from: testData)
            XCTAssert(false, "Error wasn't thrown on missing key")
        } catch DecodingError.keyNotFound(let codingKey, _) {
            XCTAssertEqual(codingKey.stringValue, "double")
        } catch {
            XCTAssert(false, "Wrong during decoding. Expected 'key not found', received \(error)")
        }
    }
    
    func testMissingValue() {
        let testData = """
                {
                    "string": "A",
                    "double": null,
                    "integer": 8,
                    "decimal": 80.6,
                    "null": null,
                    "bool": true
                }
            """.data(using: .utf8)!
        do {
            _ = try decoder.decode(Entity.self, from: testData)
            XCTAssert(false, "Error wasn't thrown on missing value")
        } catch DecodingError.valueNotFound(let type, let context) {
            XCTAssertEqual(context.codingPath.last?.stringValue, "double")
            XCTAssert(type == Double.self)
        } catch {
            XCTAssert(false, "Wrong during decoding. Expected 'value not found', received \(error)")
        }
    }
    
    func testTypeMismatch() {
        let testData = """
                {
                    "string": "A",
                    "double": 8.88,
                    "integer": "8",
                    "decimal": 80.6,
                    "null": null,
                    "bool": true
                }
            """.data(using: .utf8)!
        do {
            _ = try decoder.decode(Entity.self, from: testData)
            XCTAssert(false, "Error wasn't thrown on type mismatch")
        } catch DecodingError.typeMismatch(let type, let context) {
            XCTAssertEqual(context.codingPath.last?.stringValue, "integer")
            XCTAssert(type == Int.self)
        } catch {
            XCTAssert(false, "Wrong during decoding. Expected 'type mismatch', received \(error)")
        }
    }
    
    func testPerformance() {
        let testData = """
                [
                    {
                        "string": "A",
                        "double": 2.88,
                        "integer": 8,
                        "decimal": 80.6,
                        "null": null,
                        "bool": true
                    },
                    {
                        "string": "B",
                        "double": 2.88,
                        "integer": 8,
                        "decimal": 80.6,
                        "null": null,
                        "bool": true
                    }
                ]
            """.data(using: .utf8)!
        
        let customDecoder = decoder!
        measureMetrics([
            XCTPerformanceMetric.wallClockTime,
            XCTPerformanceMetric("com.apple.XCTPerformanceMetric_HighWaterMarkForHeapAllocations"), XCTPerformanceMetric("com.apple.XCTPerformanceMetric_HighWaterMarkForVMAllocations")
        ], automaticallyStartMeasuring: true) {
            _ = try? customDecoder.decode([Entity].self, from: testData)
        }
    }
    
    func testPerformanceWithDefaultDecoder() {
        let testData = """
                [
                    {
                        "string": "A",
                        "double": 2.88,
                        "integer": 8,
                        "decimal": 80.6,
                        "null": null,
                        "bool": true
                    },
                    {
                        "string": "B",
                        "double": 2.88,
                        "integer": 8,
                        "decimal": 80.6,
                        "null": null,
                        "bool": true
                    }
                ]
            """.data(using: .utf8)!
        
        let defaultDecoder = JSONDecoder()
        measureMetrics([
            XCTPerformanceMetric.wallClockTime,
            XCTPerformanceMetric("com.apple.XCTPerformanceMetric_HighWaterMarkForHeapAllocations"), XCTPerformanceMetric("com.apple.XCTPerformanceMetric_HighWaterMarkForVMAllocations")
        ], automaticallyStartMeasuring: true) {
            _ = try? defaultDecoder.decode([Entity].self, from: testData)
        }
    }
}
